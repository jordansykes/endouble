import React from 'react';
import ApplicationWrapper from './components/ApplicationWrapper/ApplicationWrapper';
import Hero from './components/Hero/Hero';
import './App.css';

const App = () => (
  <div className="App">
    <Hero /> 
    <ApplicationWrapper />
  </div>
);

export default App;
