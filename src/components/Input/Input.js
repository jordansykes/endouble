import React from 'react';
import './Input.css';


const Input = props => (
    <div className="input__wrapper">
        <input className="input__single"
            type={props.Type}
            name={props.Name}
            minLength={props.MinLength}
            placeholder={props.PlaceholderText}
            pattern={props.Pattern}
            onChange={(event) => props.update(event)}
            required />
    </div>
);

export default Input; 
