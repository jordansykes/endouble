import React from 'react';
import './Button.css';


const Button = props => (
    <input type="submit" className={"btn " + (props.Show ? 'hidden' : '')} value="Apply for this job"  />
);

export default Button;