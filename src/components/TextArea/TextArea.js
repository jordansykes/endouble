import React from 'react';
import './TextArea.css';


const TextArea = props => (
    <div className="textarea__wrapper">
        <label htmlFor="Your Motivation" className="textarea__label bold">Your motivation*</label> 
        <textarea className="textarea__single" value={props.ambition}minLength="100" placeholder="100 characters minimum" required></textarea>  
    </div>
);


export default TextArea;