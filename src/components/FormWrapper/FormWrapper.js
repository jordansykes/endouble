import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import Input from '../Input/Input';
import TextArea from '../TextArea/TextArea';
import Select from '../Select/Select';
import FileUpload from '../FileUpload/FileUpload';
import Button from '../Button/Button';
import AlertBox from '../AlertBox/AlertBox';

import './FormWrapper.css';
import 'react-datepicker/dist/react-datepicker.css';


export default class FormWrapper extends Component {
    
    constructor (props) {
        super(props);
        // this.handleDate = this.handleDate.bind(this);

        // Initial data thats updated on change and can be used to send a payload of data when form submits
        this.state = {
            firstName: '',
            lastName: '',
            dob:'',
            gender: '',
            email: '',
            addressOne: '',
            city: '',
            postcode: '',
            showAlertBox: false 
        }
    }
    // handle change of input fields
    updateValue = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        
        this.setState({
            [name]: value
        });
    }

    // handle change of datepicker
    handleDate = (date) => {
        this.setState({
          dob: date
        });
      }

    // prevent the form from submitting and showing the Alertbox.
    // Obviously this would not be done in a situation where the form has somewhere to be submitted too
    handleSubmit = (event) => {
        event.preventDefault();
        if(event.target.checkValidity()){ 
            this.setState({
                showAlertBox: true
            })
        }
    }

    render() {
        return(
            <form onSubmit={this.handleSubmit}>
                <div className="form__wrapper">
                    <Input update = {this.updateValue} PlaceholderText = 'First Name' Value={this.state.firstName} Name='firstName' Type="text" MinLength="2"/> 
                    <Input update = {this.updateValue} PlaceholderText = 'Last Name' Value={this.state.lastName} Name='lastName'  Type="text" MinLength="2" />
                    <div className="input__wrapper">
                    <DatePicker
                        placeholderText="Date of birth (DD/MM/YYYY)"
                        dateFormat="DD/MM/YYYY"
                        selected={this.state.dob}
                        onChange={(date) => this.handleDate(date)}
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                        required
                    />
                    </div>
                    <Select update = {this.updateValue} Gender= {this.state.gender} />
                    <Input update = {this.updateValue} PlaceholderText = 'Email Address' Value={this.state.email} Name= 'email' Type="email" MinLength="2" />
                    <Input update = {this.updateValue} PlaceholderText = 'First Line of Address' Value={this.state.addressOne} Name='addressOne' Type="text" MinLength="6" />                              
                    <Input update = {this.updateValue} PlaceholderText = 'City' Value={this.state.city} Name='city' Type="text" MinLength="2" />
                    <Input update = {this.updateValue} PlaceholderText = 'Postcode' Value={this.state.postcode} Name='postcode' MinLength="6" Type="text" Pattern="[0-9]{4}(|\s)[a-zA-Z]{2}" />
                    <TextArea />
                    <FileUpload />
                    <AlertBox Show= {this.state.showAlertBox} />
                    <div className="form__submit">
                        <Button Show= {this.state.showAlertBox} />
                    </div>
                </div>
            </form>
        );
    }
}