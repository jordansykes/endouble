import React from 'react';
import './AlertBox.css';


const AlertBox = props => (
    <div className={"alert-box__wrapper " + (props.Show ? 'active' : '')}>
        <h1>Success! We'll be in touch soon</h1>
    </div>
);

export default AlertBox;