import React from 'react';
import './Select.css';


const Select = props => (
    <div className="select__wrapper">
        <select name="gender" id="" className="select__picker" value={props.Gender} onChange={(event) => props.update(event)} required>
            <option value="" className="select__single" disabled>Please select gender</option>                   
            <option value="Male" className="select__single">Male</option>
            <option value="Female" className="select__single">Female</option>
            <option value="Other" className="select__single">Other</option>
        </select>
    </div>
);

export default Select;