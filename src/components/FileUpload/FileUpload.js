import React from 'react';
import Computer from '../../assets/img/computer.svg';

import Tick from '../../assets/img/tick.svg';
import './FileUpload.css';


const FileUpload = () => (
    <div className="upload__wrapper">
        <label htmlFor="file" className="upload__title bold">Attach your documents <span>(pdf, doc(x), rtf, txt)</span></label>
        <div className="upload__single">
            <input type="file" name="file" id="file" className="upload__file" accept=".doc, .docx, .pdf, .rtf, .txt" required />
            <p className="upload__sub-title">Resume* <img src={Tick} className="upload__tick" alt=""/></p>
            <label htmlFor="file" className="upload__label"><img src={Computer} className="upload__comp" alt=""/> Upload</label>
        </div>
    </div>
);


export default FileUpload;