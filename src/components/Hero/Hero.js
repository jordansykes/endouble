import React from 'react';
import './Hero.css';


const Hero = () => (
    <div className="hero">
        <div className="hero__plaque">
            <h1 className="hero__title">Purchasing Assistant</h1>
            <h2 className="hero__sub-title">Amsterdam, The Netherlands</h2>
        </div>
    </div>
);

export default Hero;