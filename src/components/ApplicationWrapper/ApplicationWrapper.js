import React from 'react';
import FormWrapper from '../FormWrapper/FormWrapper';
import './ApplicationWrapper.css';


const ApplicationWrapper = () => (
    <section className="application__wrapper">
        <h1 className="application__title">apply for the position of purchasing assistant</h1>
        <FormWrapper />
    </section>
);

export default ApplicationWrapper;